# Network Logger Chrome Extension

## Intro

Network Logger Chrome Extension

### Requirements

* Chrome browser

### Install

1. Clone the project
2. On chrome browser, click `Manage extensions`
3. Click `Load unpacked`
4. Select the newly cloned project

### Usage

1. On chrome browser, click `Manage extensions`
2. Toggle the chrome extension
3. Click `background page` link to view logs
